/*jslint vars: true, plusplus: true, devel: true, nomen: true, indent: 4, maxerr: 50 */
/*global define, $, brackets, window */

define(function (require, exports, module) {
	
    "use strict";

    var CommandManager    = brackets.getModule("command/CommandManager"),
		KeyBindingManager = brackets.getModule("command/KeyBindingManager");

    function switchPane() {
		var $firstPane  = $('#first-pane'),
			$secondPane = $('#second-pane');
        if ($firstPane.hasClass('active-pane')) {
			$secondPane.click();
		} else {
			$firstPane.click();
		}
    }

    var switchCommand = "moving-between-splitted-windows.switch";
    CommandManager.register("Switch active pane", switchCommand, switchPane);

	KeyBindingManager.addBinding(switchCommand, {key: 'Alt-W'});
	
});