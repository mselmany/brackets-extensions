RightClickExtended
==================

Adds in Cut, Copy and Paste functions to the right click menu in Brackets.io.

It uses the node-copy-paste library for accessing the clipboard: https://github.com/xavi-/node-copy-paste

## Change log
* 1.2.0
  * Cursor now sits at the end of pasted content
  * No longer copies blank selections


## Developed by
* Jarrad Salmon

## License
This project is released under [The MIT License](http://www.opensource.org/licenses/mit-license.php).
